package main

import (
	"github.com/gorilla/mux"
	"gorm-rest-api/routes"
	"log"
	"net/http"
)

func main() {
	router := mux.NewRouter()

	router.HandleFunc("/", routes.HomeHandler)

	err := http.ListenAndServe(":3000", router)
	if err != nil {
		log.Fatal("error creando el server")
	}
}
